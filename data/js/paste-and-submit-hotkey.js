function getAncestorByTagName(node, tag){
    for(tag = tag.toUpperCase(); node && node.tagName !== tag; node = node.parentNode){}
    return node;
}

let node = document.activeElement;

if (node) {
    let nodeName = node.tagName.toLowerCase();

    if (node.nodeType == 1 &&
        (nodeName == "textarea" || (nodeName == "input" && /^(?:text|email|number|search|tel|url|password)$/i.test(node.type)))) {

        var form = getAncestorByTagName(node, "form");
        if (form)
            self.postMessage();
    }
}

self.on("message", function(data) {
    var strPos = node.selectionStart;
    node.value = node.value.substring(0, strPos) + data + node.value.substring(node.selectionEnd, node.value.length);
    node.selectionStart = node.selectionEnd = strPos + data.length;
    node.focus();
    form.submit();
});
